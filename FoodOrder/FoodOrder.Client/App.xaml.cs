﻿using FoodOrder.Client.Model;
using FoodOrder.Client.View;
using FoodOrder.Client.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace FoodOrder.Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private FoodOrderModel model;
        private FoodOrderViewModel mainViewModel;
		private LoginViewModel loginViewModel;
		private ItemsViewModel itemsViewModel;
		private OrdersViewModel ordersViewModel;
        private MainWindow mainwindow;
        private LoginWindow loginwindow;
        private ItemEditorWindow itemwindow;
        private OrderDetails orderdetails;

        public App()
        {
			Startup += App_Startup;
		}

		private void App_Startup(object sender, StartupEventArgs e)
		{
			var baseUrl = ConfigurationManager.AppSettings["BaseUrl"];
            model = new FoodOrderModel(baseUrl);
            mainViewModel = new FoodOrderViewModel(model);
            loginViewModel = new LoginViewModel(model);
            ordersViewModel = new OrdersViewModel(model);
            itemsViewModel = new ItemsViewModel(model);
			mainwindow = new MainWindow() { DataContext=mainViewModel};
			loginwindow = new LoginWindow() { DataContext = loginViewModel };
			itemwindow = new ItemEditorWindow() { DataContext = itemsViewModel };
            orderdetails = new OrderDetails() { DataContext = ordersViewModel };

            loginViewModel.loginok += LoginSucc;
            loginViewModel.loginfailed += LoginFail;
            mainViewModel.loggedout += LogoutProc;
			mainViewModel.failed += ShowError;
			itemsViewModel.failed += ShowError;
			ordersViewModel.failed += ShowError;
            mainViewModel.showdetailswindow += ShowDetails;
            ordersViewModel.closedetailswindow += CloseDetails;
            itemsViewModel.callapptohideeditwindow += CloseEdit;
            mainViewModel.callapptoshowitemwindow += ShowEdit;

            orderdetails.Closing += ReshowMainWindow;
            itemwindow.Closing += ReshowMainWindow2;

            loginwindow.Show();
		}

		private async void ShowEdit(object? sender, EventArgs e)
		{
			mainwindow.Hide();
			await itemsViewModel.RefreshItemData();
			itemwindow.Show();
		}

		private void ReshowMainWindow2(object? sender, CancelEventArgs e)
		{
			e.Cancel = true;
			itemwindow.Hide();
			mainwindow.Show();
		}

		private void CloseEdit(object? sender, EventArgs e)
		{
			itemwindow.Hide();
			mainwindow.Show();
		}

		private async void ReshowMainWindow(object? sender, CancelEventArgs e)
		{
			e.Cancel = true;
            orderdetails.Hide();
			await mainViewModel.RefreshOrders();
            mainwindow.Show();
		}

		private void CloseDetails(object? sender, EventArgs e)
		{
            
			ReshowMainWindow(sender, new CancelEventArgs());
		}

		private async void ShowDetails(object? sender, int id)
		{
            mainwindow.Hide();
            await ordersViewModel.RefreshData(id);
            orderdetails.Show();
		}

		private void ShowError(object? sender, string e)
		{
			MessageBox.Show(e);
		}

		private void LoginFail(object? sender, string e)
		{
            MessageBox.Show(e);
		}

		private async void LoginSucc(object? sender, EventArgs e)
		{
            loginwindow.Hide();
            await mainViewModel.RefreshOrders();
            mainwindow.Show();
		}

		private void LogoutProc(object? sender, EventArgs e)
		{
            mainwindow.Hide();
            loginwindow.Show();
		}
	}
}
