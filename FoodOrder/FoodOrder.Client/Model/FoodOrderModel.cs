﻿using FoodOrder.DTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Xml.Linq;

namespace FoodOrder.Client.Model
{
	public class FoodOrderModel
	{
		private HttpClient client;
		public EventHandler<string> failed;

		public List<OrderDTO> orders;
		public OrderDTO order;
		public List<CategoryDTO> categories;

		public string name;
		public string address;
		public string phoneNumber;
		public string fulfilled;
		public DateTime? fulfilledTime;
		public DateTime? recordedTime;
		private int id = -1;
		public int sum = 0;
		public FoodOrderModel(string baseUrl)
		{
			client = new HttpClient()
			{
				BaseAddress = new Uri(baseUrl)
			};
		}
		public async Task<bool> Login(string loginName, string password)
		{
			ApplicationUserDTO user = new ApplicationUserDTO
			{
				UserName = loginName,
				Password = password
			};


			try
			{
				HttpResponseMessage response = await client.PostAsJsonAsync("Account/Login", user);
				if (response.IsSuccessStatusCode)
				{
					return true;
				}
				return false;
			}
			catch
			{
				return false;
			}
		}
		public async Task<bool> LogoutAsync()
		{
			try
			{
				HttpResponseMessage response = await client.PostAsync("Account/Logout", null);

				if (response.IsSuccessStatusCode)
				{
					return true;
				} else
				{
					throw new Exception();
				}
			}
			catch
			{
				failed?.Invoke(this, "Could not resolve Logout!");
				return false;
			}
		}

		public async Task GetOrdersAsync()
		{
			try
			{
				HttpResponseMessage response = await client.GetAsync("Orders/GetOrders");

				if (response.IsSuccessStatusCode)
				{
					orders = await response.Content.ReadAsAsync<List<OrderDTO>>();
				}
				else
				{
					orders = new List<OrderDTO>();
					failed?.Invoke(this, "Unable to get Orders!");
				}
			}
			catch (Exception)
			{
			}
		}

		public async Task GetOrderById(int param)
		{
			try
			{
				HttpResponseMessage response = await client.GetAsync($"Orders/GetOrder/{param}");

				if (!response.IsSuccessStatusCode)
				{
					failed?.Invoke(this, "Unable to get details of choosen order!");
					return;
				}
				sum = 0;
				order = await response.Content.ReadAsAsync<OrderDTO>();
			}
			catch (Exception)
			{
			}
			foreach (var item in order.Items)
			{
				sum += item.Price;
			}
			if (order != null)
			{
				id = order.Id;
				name = order.Name;
				address = order.Address;
				fulfilledTime = order.FulfilledTime;
				recordedTime = order.RecordedTime;
				phoneNumber = order.PhoneNumber;
				fulfilled = order.Fulfilled ? "fulfilled" : "not fulfilled";
			}
		}

		public async Task FulfilSelectedOrder()
		{
			try
			{
				HttpResponseMessage response = await client.PutAsync($"Orders/PutOrderCompleted/{id}", null);

				if (!response.IsSuccessStatusCode)
				{
					failed?.Invoke(this, "Unable to fulfil order!");
					return;
				}
			}
			catch (Exception)
			{
				failed?.Invoke(this, "Failed while trying to reach server!");
			}
		}
		public async Task<bool> AddNewItem(string itemName, string itemDescription, int itemPrice, int itemCategory,bool itemspicy, bool itemvegetarian)
		{
			ItemDTO dto = new ItemDTO()
			{
				Name = itemName,
				Description = itemDescription,
				Price = itemPrice,
				CategoryId = itemCategory,
				Vegetarian = itemvegetarian,
				Spicy= itemspicy,
			};
			try
			{
				HttpResponseMessage response = await client.PostAsJsonAsync($"Items/PostItem", dto);

				if (!response.IsSuccessStatusCode)
				{
					return false;
				}
				return true;
			}
			catch (Exception)
			{

				return false;
			}
		}

		public async Task GetCategories()
		{
			try
			{
				HttpResponseMessage response = await client.GetAsync($"Items/GetCategories");

				if (!response.IsSuccessStatusCode)
				{
					categories = new List<CategoryDTO>();
					return;
				}
				categories = await response.Content.ReadAsAsync<List<CategoryDTO>>();
			}
			catch (Exception)
			{
			}
		}
	}
}
