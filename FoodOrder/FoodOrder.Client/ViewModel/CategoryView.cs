﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodOrder.Client.ViewModel
{
	public class CategoryView
	{
		public string? Name { get; set; }
		public int Id { get; set; }
		public DelegateCommand SetCategoryCommand { get; set; } = null!;
	}
}
