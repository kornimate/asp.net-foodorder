﻿using FoodOrder.Client.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace FoodOrder.Client.ViewModel
{
    public class FoodOrderViewModel : ViewModelBase
    {
        private FoodOrderModel model;
        public string Query { get; set; }
        public ObservableCollection<OrderView> NotFulfilledOrders
        {
            get
            {
                if (Query is null || Query == "")
                {
                    if (model.orders is null || model.orders.Count == 0) { return new ObservableCollection<OrderView>(); }
                    return new ObservableCollection<OrderView>(model.orders.Where(x => !x.Fulfilled).Select(x => new OrderView
                    {
                        OrderData = x.Name + " | " + x.Address + " | " + (x.Fulfilled ? "✅" : "❌"),
                        Id = x.Id,
                        OrderSelectedCommand = new DelegateCommand((param) => ShowOrderDetails(param))
                    }).ToList());
                }
                else
                    return new ObservableCollection<OrderView>(model.orders.Where(x => !x.Fulfilled && (x.Name.ToLower().Contains(Query.ToLower()) || x.Address.ToLower().Contains(Query.ToLower()))).Select(x => new OrderView
                    {
                        OrderData = x.Name + " | " + x.Address + " | " + (x.Fulfilled ? "✅" : "❌"),
                        Id = x.Id,
                        OrderSelectedCommand = new DelegateCommand((param) => ShowOrderDetails(param))
                    }).ToList());
            }
        }

        public ObservableCollection<OrderView> FulfilledOrders
        {
            get
            {
                if (Query is null || Query == "")
                {
                    if (model.orders is null || model.orders.Count == 0) { return new ObservableCollection<OrderView>(); }
                    return new ObservableCollection<OrderView>(model.orders.Where(x => x.Fulfilled).Select(x => new OrderView
                    {
                        OrderData = x.Name + " | " + x.Address + " | " + (x.Fulfilled ? "✅" : "❌"),
                        Id = x.Id,
                        OrderSelectedCommand = new DelegateCommand((param) => ShowOrderDetails(param))
                    }).ToList());
                }
                else
                    return new ObservableCollection<OrderView>(model.orders.Where(x => x.Fulfilled && (x.Name.ToLower().Contains(Query.ToLower()) || x.Address.ToLower().Contains(Query.ToLower()))).Select(x => new OrderView
                    {
                        OrderData = x.Name + " | " + x.Address + " | " + (x.Fulfilled ? "✅" : "❌"),
                        Id = x.Id,
                        OrderSelectedCommand = new DelegateCommand((param) => ShowOrderDetails(param))
                    }).ToList());

            }
        }

        public DelegateCommand RefreshOrdersCommand { get; set; }
        public DelegateCommand LogoutCommand { get; set; }
        public DelegateCommand OrderSelectedCommand { get; set; }
        public DelegateCommand AddItemCommand { get; set; }
        public DelegateCommand SearchCommand { get; set; }


        public EventHandler loggedout;
        public EventHandler<string> failed;
        public EventHandler<int> showdetailswindow;

        public EventHandler callapptoshowitemwindow;

        public FoodOrderViewModel(FoodOrderModel model)
        {
            this.model = model;

            model.failed += Failed;

            LogoutCommand = new DelegateCommand(async (param) => await Logout());
            RefreshOrdersCommand = new DelegateCommand(async (param) => await RefreshOrders());
            SearchCommand = new DelegateCommand((param) => Search());
            AddItemCommand = new DelegateCommand((param) => ShowItemWindow());

        }

        private void ShowItemWindow()
        {
            callapptoshowitemwindow?.Invoke(this, EventArgs.Empty);
        }
        private void Search()
        {
            OnPropertyChanged(nameof(NotFulfilledOrders));
            OnPropertyChanged(nameof(FulfilledOrders));
        }

        private void ShowOrderDetails(object param)
        {
            showdetailswindow?.Invoke(this, (int)param);
        }

        public async Task RefreshOrders()
        {
            await model.GetOrdersAsync();
            OnPropertyChanged(nameof(FulfilledOrders));
            OnPropertyChanged(nameof(NotFulfilledOrders));
        }

        private void Failed(object? sender, string e)
        {
            failed?.Invoke(this, e);
            OnPropertyChanged(nameof(NotFulfilledOrders));
            OnPropertyChanged(nameof(FulfilledOrders));
        }

        private async Task Logout()
        {
            bool result = await model.LogoutAsync();
            if (result)
            {
                loggedout?.Invoke(this, EventArgs.Empty);
            }
        }
    }
}
