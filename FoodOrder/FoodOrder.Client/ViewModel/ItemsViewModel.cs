﻿using FoodOrder.Client.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodOrder.Client.ViewModel
{
    public class ItemsViewModel : ViewModelBase
    {
        private FoodOrderModel model;
		public string ItemName { get; set; }
		public string ItemDescription { get; set; }
		public string ItemPrice { get; set; }
		public bool ItemSpicy { get; set; }
		public bool ItemVegetarian { get; set; }
		public int ItemCategory { get; set; }
		public string CatName { get; set; }
		public DelegateCommand SaveItemAddCommand { get; set; }
		public DelegateCommand SetCategoryCommand { get; set; }
		public DelegateCommand CancelItemAddCommand { get; set; }

		public EventHandler<string> failed;
		public EventHandler callapptohideeditwindow;
		public ObservableCollection<CategoryView> Categories
		{
			get
			{
                if (model.categories is null || model.categories.Count == 0) { return new ObservableCollection<CategoryView>(); }
                return new ObservableCollection<CategoryView>(model.categories.Select(x => new CategoryView
				{
					Id = x.Id,
					Name = x.Name,
					SetCategoryCommand = new DelegateCommand((param) => SetCategory(param))
				}));
			}
		}
		public ItemsViewModel(FoodOrderModel model)
		{
			this.model = model;
			SaveItemAddCommand = new DelegateCommand(async (param) => await AddNewItem());
		}
		private void SetCategory(object param)
		{
			ItemCategory = (int)param;
			CatName = (model.categories.Single(x => x.Id == ItemCategory)).Name;
			OnPropertyChanged(nameof(CatName));
		}
		private async Task AddNewItem()
		{
			if (ItemName is null || ItemName == "")
			{
				failed?.Invoke(this, "Name Error!");
				return;
			}
			if (ItemDescription is null)
			{
				failed?.Invoke(this, "Description Error!");
				return;
			}
			if (ItemPrice == "" || int.TryParse(ItemPrice, out int price) == false)
			{
				failed?.Invoke(this, "Price Error, Invalid Format!");
				return;
			}
			if(price < 0)
			{
				failed?.Invoke(this, "Price Error, Negative price!");
				return;
			}
			if (ItemCategory == -1)
			{
				failed?.Invoke(this, "Category Error!");
				return;
			}
			var ok = await model.AddNewItem(ItemName, ItemDescription, price, ItemCategory, ItemSpicy, ItemVegetarian);
			if (!ok) { failed?.Invoke(this, "Can not add item to database (It may already exist)!"); return; }
			callapptohideeditwindow?.Invoke(this, EventArgs.Empty);
		}
		public async Task RefreshItemData()
		{
			ItemCategory = -1;
			ItemName = "";
			ItemPrice = "";
			ItemDescription = "";
			ItemVegetarian = false;
			ItemSpicy = false;
			CatName = "";
			OnPropertyChanged(nameof(ItemCategory));
			OnPropertyChanged(nameof(ItemPrice));
			OnPropertyChanged(nameof(ItemDescription));
			OnPropertyChanged(nameof(ItemSpicy));
			OnPropertyChanged(nameof(ItemVegetarian));
			OnPropertyChanged(nameof(CatName));
			OnPropertyChanged(nameof(ItemName));
			await model.GetCategories();
			OnPropertyChanged(nameof(Categories));
		}
	}
}
