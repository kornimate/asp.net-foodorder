﻿using FoodOrder.Client.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace FoodOrder.Client.ViewModel
{
	public class LoginViewModel : ViewModelBase
	{
		private FoodOrderModel model;
		public string? LoginName { get; set; }
		public string? Password { get; set; }
		public DelegateCommand LoginCommand { get; set; }

		public EventHandler loginok;
		public EventHandler<string> loginfailed;
		public LoginViewModel(FoodOrderModel model)
		{
			this.model = model;
			LoginCommand = new DelegateCommand(async (param) => await Login(param));
		}

		private async Task Login(object param)
		{
			if(LoginName is null || LoginName == "")
			{
				loginfailed?.Invoke(this, "No User Name, it is compulsory!");
				return;
			}
			if (param is null)
			{
				loginfailed?.Invoke(this, "Something went wrong with the Password!");
				return;
			}
			bool res = await model.Login(LoginName, ((PasswordBox)param).Password);
			if (!res)
			{
				loginfailed?.Invoke(this, "Failed to Login");
			}
			else
			{
				loginok?.Invoke(this, new EventArgs());
			}
		}
	}
}
