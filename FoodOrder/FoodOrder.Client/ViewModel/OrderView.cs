﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodOrder.Client.ViewModel
{
	public class OrderView
	{
		public string? OrderData { get; set; }
		public int Id { get; set; }
		public DelegateCommand OrderSelectedCommand { get; set; } = null!;
	}
}
