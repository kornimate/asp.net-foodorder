﻿using FoodOrder.Client.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodOrder.Client.ViewModel
{
    public class OrdersViewModel : ViewModelBase
    {
        private FoodOrderModel model;
		public DelegateCommand FulfilOrderCommand { get; set; }

		public EventHandler<string> failed;
		public EventHandler closedetailswindow;
		public string Name
		{
			get
			{
				return model.name;
			}
		}
		public string Address
		{
			get
			{
				return model.address;
			}
		}

		public string PhoneNumber
		{
			get
			{
				return model.phoneNumber;
			}
		}

		public string FulfilledTime
		{
			get
			{
				if (model.fulfilledTime == null) { return "not yet"; }
				return model.fulfilledTime.ToString()!;
			}
		}

		public string RecordedTime
		{
			get
			{
				if (model.recordedTime == null) { return "not yet"; }
				return model.recordedTime.ToString()!;
			}
		}
		public string Fulfilled
		{
			get
			{
				return model.fulfilled;
			}
		}
		public ObservableCollection<ItemView> SItems
		{
			get
			{
				if(model.order == null) { return null!; }
				return new ObservableCollection<ItemView>(model.order.Items.Select(x => new ItemView { DataName = x.Name }));
			}
		}

		public string Sum
		{
			get
			{
				return model.sum.ToString() + "$";
			}
		}
		public OrdersViewModel(FoodOrderModel model)
		{
			this.model = model;
			FulfilOrderCommand = new DelegateCommand(async (param) => await FulfilOrder());
		}
		private async Task FulfilOrder()
		{
			if (Fulfilled == "fulfilled")
			{
				failed?.Invoke(this, "This order is already fulfilled!");
				return;
			}
			await model.FulfilSelectedOrder();
			closedetailswindow?.Invoke(this, EventArgs.Empty);
		}
		public async Task RefreshData(int id)
		{
			await model.GetOrderById(id);
			OnPropertyChanged(nameof(Name));
			OnPropertyChanged(nameof(Address));
			OnPropertyChanged(nameof(PhoneNumber));
			OnPropertyChanged(nameof(RecordedTime));
			OnPropertyChanged(nameof(FulfilledTime));
			OnPropertyChanged(nameof(Fulfilled));
			OnPropertyChanged(nameof(Sum));
			OnPropertyChanged(nameof(SItems));
		}
	}
}
