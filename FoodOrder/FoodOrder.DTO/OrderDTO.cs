﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoodOrder.DTO
{
	public class OrderDTO
	{
		public int Id { get; set; }
		public string Name { get; set; } = null!;
		public string Address { get; set; } = null!;
		public string PhoneNumber { get; set; } = null!;
		public bool Fulfilled { get; set; }
		public DateTime RecordedTime { get; set; }
		public DateTime? FulfilledTime { get; set; }
		public List<ItemDTO> Items { get; set; } = null!;
	}
}
