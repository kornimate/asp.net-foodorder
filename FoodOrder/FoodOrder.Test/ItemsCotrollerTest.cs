﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using AutoMapper;
using FoodOrder.WebApi.MappingConfigurations;
using System.Diagnostics;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace FoodOrder.Tests
{
    public class ItemsControllerTest : IDisposable
    {
        private readonly FoodOrderDbContext _context;
        private readonly FoodOrderService _service;
        private readonly ItemsController _controller;
        private readonly IMapper _mapper;
        private readonly ITestOutputHelper output;

        public ItemsControllerTest(ITestOutputHelper output)
        {
            var options = new DbContextOptionsBuilder<FoodOrderDbContext>()
                .UseInMemoryDatabase("TestDb2")
                .Options;

            _context = new FoodOrderDbContext(options);
            TestDbInitializer.Initialize(_context);

            _context.ChangeTracker.Clear();
            _service = new FoodOrderService(_context);

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new CategoryProfile());
                cfg.AddProfile(new CategoryDTOProfile());
                cfg.AddProfile(new ItemProfile());
                cfg.AddProfile(new ItemDTOProfile());
            });
            _mapper = new Mapper(config);
            _controller = new ItemsController(_service, _mapper);

            this.output = output;  
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [Fact]
        public void GetCategoriesTest()
        {
            var result = _controller.GetCategories();
            var content = Assert.IsAssignableFrom<IEnumerable<CategoryDTO>>(result.Value);
            Assert.Equal(4, content.Count());
        }

        [Fact]
        public void PostItemTest()
        {
            ItemDTO dto = new ItemDTO
            {
                Name = "TestItem",
                Description = "Description",
                Price = 1300,
                Spicy = true,
                Vegetarian = false,
                CategoryId = 1
            };
            var result = _controller.PostItem(dto);
            Assert.IsAssignableFrom<CreatedAtActionResult>(result);
        }

        [Fact]
		public void PostItemFailTest()
		{
			ItemDTO dto = new ItemDTO
			{
				Name = "Water", //existing item
				Description = "Description",
				Price = 1300,
				Spicy = true,
				Vegetarian = false,
				CategoryId = 1
			};
			var result = _controller.PostItem(dto);
			Assert.IsAssignableFrom<BadRequestResult>(result);
		}
	}
}
