﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using AutoMapper;
using FoodOrder.WebApi.MappingConfigurations;
using System.Diagnostics;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace FoodOrder.Tests
{
    public class OrdersControllerTest : IDisposable
    {
        private readonly FoodOrderDbContext _context;
        private readonly FoodOrderService _service;
        private readonly OrdersController _controller;
        private readonly IMapper _mapper;
        private readonly ITestOutputHelper output;

        public OrdersControllerTest(ITestOutputHelper output)
        {
            var options = new DbContextOptionsBuilder<FoodOrderDbContext>()
                .UseInMemoryDatabase("TestDb")
                .Options;

            _context = new FoodOrderDbContext(options);
            TestDbInitializer.Initialize(_context);

            _context.ChangeTracker.Clear();
            _service = new FoodOrderService(_context);

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new OrderProfile());
                cfg.AddProfile(new OrderDTOProfile());
            });
            _mapper = new Mapper(config);
            _controller = new OrdersController(_service, _mapper);

            this.output = output;  
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }

        [Fact]
        public void GetOrdersTest()
        {
            var result = _controller.GetOrders();
            var content = Assert.IsAssignableFrom<IEnumerable<OrderDTO>>(result.Value);
            Assert.Equal(3, content.Count());
        }

		[Theory]
		[InlineData(1)]
		[InlineData(2)]
		[InlineData(3)]
		public void GetOrderByIdTest(int id)
        {
			var result = _controller.GetOrder(id);
            var content = Assert.IsAssignableFrom<OrderDTO>(result.Value);
            Assert.Equal(id, content.Id);
        }

        [Fact]
        public void GetInvalidOrdersTest()
        {
            var id = 100;
            var result = _controller.GetOrder(id);
            Assert.IsAssignableFrom<NotFoundResult>(result.Result);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        public void FulfilOrderTest(Int32 id)
        {
            var result = _controller.PutOrderCompleted(id);
            var requestResult = Assert.IsAssignableFrom<OkResult>(result);
            var updatedOrder = _controller.GetOrder(id);
            Assert.Equal(true,updatedOrder?.Value?.Fulfilled);
        }

        [Fact]
        public void FulfilOrderFailTest()
        {
            int id = 100;
            var result = _controller.PutOrderCompleted(id);
            var requestResult = Assert.IsAssignableFrom<NotFoundResult>(result);
        }
    }
}
