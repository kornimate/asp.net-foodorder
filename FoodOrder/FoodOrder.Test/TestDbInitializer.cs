﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FoodOrder.Tests
{
	public static class TestDbInitializer
	{
		public static void Initialize(FoodOrderDbContext context)
		{
			List<Category> deflists = new List<Category>()
			{
				new Category()
				{
					Name="Soups",
					Items = new List<Item>()
					{
						new Item()
						{
							Name="Fruit Soup",
							Description="A regular fruit soup",
							Price=2100,
							Spicy=false,
							Vegetarian=true,
						},
						new Item()
						{
							Name="Vegetable Soup",
							Description="A regular vegetable soup",
							Price=1500,
							Spicy=false,
							Vegetarian=true,
						},
						new Item()
						{
							Name="Chicken Soup",
							Description="A regular chicken soup",
							Price=2000,
							Spicy=false,
							Vegetarian=false,
						}
					}
				},
				new Category()
				{
					Name="Still Drinks",
					Items = new List<Item>()
					{
						new Item()
						{
							Name="Water",
							Description="Simple still water",
							Price=300,
							Spicy=false,
							Vegetarian=true,
						},
						new Item()
						{
							Name="Orange Juice",
							Description="Made from fresh oranges",
							Price=900,
							Spicy=false,
							Vegetarian=true,
						},
						new Item()
						{
							Name="Apple juice",
							Description="Made from fresh apples",
							Price=900,
							Spicy=false,
							Vegetarian=true,
						}
					}
				},
				new Category()
				{
					Name="Main Dishes",
					Items = new List<Item>()
					{
						new Item()
						{
							Name="Beef with French Fries",
							Description="A huge amount of food for hardly any money",
							Price=9000,
							Spicy=false,
							Vegetarian=false,
						},
						new Item()
						{
							Name="Steak",
							Description="The best Steak in Town",
							Price=15000,
							Spicy=false,
							Vegetarian=false,
						},
						new Item()
						{
							Name="Tofu and rice",
							Description="A good choice for vegetarians",
							Price=8000,
							Spicy=false,
							Vegetarian=true,
						}
					}
				},
				new Category()
				{
					Name="Desserts",
					Items = new List<Item>()
					{
						new Item()
						{
							Name="Ice Cream",
							Description="Best in Summer",
							Price=100,
							Spicy=false,
							Vegetarian=true,
						},
						new Item()
						{
							Name="Pancake",
							Description="Good after every meal",
							Price=1500,
							Spicy=false,
							Vegetarian=true,
						},
						new Item()
						{
							Name="Hot Chili Lemon Cake",
							Description="Freshy drink from out kitchen",
							Price=2000,
							Spicy=true,
							Vegetarian=true,
						}
					}
				}
			};

			context.AddRange(deflists);
			context.SaveChanges();

			List<Order> orders = new List<Order>()
			{
				new Order()
				{
					Name = "TestOrder1",
					Address="No city, No way , 1234",
					PhoneNumber="+0000000000",
					Fulfilled = false,
					RecordedTime = DateTime.Now,
					FulfilledTime = null,
					Items = (List<Item>)deflists[0].Items
				},
				new Order()
				{
					Name = "TestOrder2",
					Address="No city, No way , 1234",
					PhoneNumber="+0000000000",
					Fulfilled = false,
					RecordedTime = DateTime.Now,
					FulfilledTime = null,
					Items = (List<Item>)deflists[1].Items
				},
				new Order()
				{
					Name = "TestOrder3",
					Address="No city, No way , 1234",
					PhoneNumber="+0000000000",
					Fulfilled = false,
					RecordedTime = DateTime.Now,
					FulfilledTime = null,
					Items = (List<Item>)deflists[2].Items
				},
			};

			context.AddRange(orders);
			context.SaveChanges();
		}
	}
}
