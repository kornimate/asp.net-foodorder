global using Xunit;
global using FoodOrder.DTO;
global using FoodOrder.Shared.Models;
global using FoodOrder.Shared.Services;
global using FoodOrder.WebApi.Controllers;