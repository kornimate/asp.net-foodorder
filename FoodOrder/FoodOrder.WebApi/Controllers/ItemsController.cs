﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using FoodOrder.Shared.Services;
using FoodOrder.Shared.Models;
using FoodOrder.DTO;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace FoodOrder.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize]
    public class ItemsController : ControllerBase
    {
        private readonly IFoodOrderService _service;
        private readonly IMapper _mapper;

        public ItemsController(IFoodOrderService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [Authorize]
        [HttpPost]
        public ActionResult PostItem(ItemDTO itemDto)
        {
            var item = _mapper.Map<Item>(itemDto);
            var newItem = _service.CreateItem(item,out bool existing);

            if (newItem is null)
            {
                if(existing)
                {
                    return BadRequest();
				}
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            else
            {
                return CreatedAtAction(nameof(PostItem), new { id = item.Id },
                    _mapper.Map<ItemDTO>(item));
            }
        }

        [HttpGet]

        public ActionResult<List<CategoryDTO>> GetCategories()
        {
            return _mapper.Map<List<CategoryDTO>>(_service.GetCategories());
        }
    }
}
