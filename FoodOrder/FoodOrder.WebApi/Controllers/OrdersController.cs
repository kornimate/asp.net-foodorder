﻿using AutoMapper;
using FoodOrder.DTO;
using FoodOrder.Shared.Models;
using FoodOrder.Shared.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace FoodOrder.WebApi.Controllers
{
	[Route("/api/[controller]/[action]")]
	[ApiController]
	[Authorize]
	public class OrdersController : ControllerBase
	{
		private readonly IFoodOrderService _service;
		private readonly IMapper _mapper;
		// GET: OrdersControlles

		public OrdersController(IFoodOrderService service, IMapper mapper)
		{
			_service = service;
			_mapper = mapper;
		}
		[HttpGet]
		public ActionResult<List<OrderDTO>> GetOrders()
		{
			return _service.GetOrders().Select (x => _mapper.Map<OrderDTO>(x)).ToList();
		}
		[HttpGet("{orderId}")]
		public ActionResult<OrderDTO> GetOrder(int orderId)
		{
			Order data = _service.GetOrderDetails(orderId);
			if (data is null) return NotFound();
			OrderDTO dto = new OrderDTO
			{
				Id = data.Id,
				Name = data.Name,
				Address = data.Address,
				PhoneNumber = data.PhoneNumber,
				Fulfilled = data.Fulfilled,
				RecordedTime = data.RecordedTime,
				FulfilledTime = data.FulfilledTime,
				Items = data.Items.Select(x => new ItemDTO()
				{
					Id = x.Id,
					Name = x.Name,
					Description = x.Description,
					CategoryId = x.CategoryId,
					Spicy = x.Spicy,
					Vegetarian = x.Vegetarian,
					Price = x.Price,
				}).ToList(),
			};
			return dto;
		}

		[HttpPut("{orderId}")]
		public ActionResult PutOrderCompleted (int orderId)
		{
			if (_service.SetOrderCompleted(orderId))
			{
				return Ok();
			}
			return NotFound();
		}
	}
}
