﻿using AutoMapper;
using FoodOrder.Shared.Models;
using FoodOrder.DTO;

namespace FoodOrder.WebApi.MappingConfigurations
{
    public class ItemProfile : Profile
    {
        public ItemProfile()
        {
            CreateMap<Item, ItemDTO>();
        }
    }

	public class ItemDTOProfile : Profile
	{
		public ItemDTOProfile()
		{
			CreateMap<ItemDTO, Item>();
		}
	}

	public class OrderDTOProfile : Profile
	{
		public OrderDTOProfile()
		{
			CreateMap<OrderDTO, Order>();
		}
	}
	public class OrderProfile : Profile
	{
		public OrderProfile()
		{
			CreateMap<Order, OrderDTO>();
		}
	}

	public class CategoryDTOProfile : Profile
	{
		public CategoryDTOProfile()
		{
			CreateMap<CategoryDTO, Category>();
		}
	}
	public class CategoryProfile : Profile
	{
		public CategoryProfile()
		{
			CreateMap<Category, CategoryDTO>();
		}
	}
}
