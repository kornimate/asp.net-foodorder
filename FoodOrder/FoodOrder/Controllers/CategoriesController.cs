﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FoodOrder.Models;
using FoodOrder.Shared.Services;
using FoodOrder.Shared.Models;

namespace FoodOrder.Controllers
{
    public class CategoriesController : Controller
    {
        private readonly IFoodOrderService service;

        public CategoriesController(IFoodOrderService service)
        {
            this.service = service;
        }

        // GET: Categories
        public IActionResult Index()
        {
            try
            {
                var list = service.GetCategories();
                TempData["filterString"] = null;
                List<Tuple<string, int, int>> MostPopular = new List<Tuple<string, int, int>>();
                foreach (Category cat in list)
                {
                    foreach (Item item in cat.Items)
                    {
                        MostPopular.Add(new Tuple<string, int, int>(item.Name, item.Id, item.Orders.Count));
                    }
                }
                MostPopular = MostPopular.OrderByDescending(x => x.Item3).Take(10).ToList();
                return View(new CategoryViewModel()
                {
                    Categories = list,
                    MostPopular = MostPopular
                });
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        public IActionResult Details(int id)
        {
            try
            {
                Category list = service.GetCategoryDetails(id);
                if (TempData.Peek("filterString") != null)
                {
                    string filterString = (string)TempData.Peek("filterString")!;
                    list.Items = list.Items.Where(x => x.Name.ToLower().Contains(filterString.Trim().ToLower())).ToList();
                }
                return View(list);
            }
            catch
            {
                return NotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Details(int id, string filterString="")
        {
            try
            {
                Category category = service.GetCategoryDetails(id);
                if (filterString != null && filterString!="")
                {
                    category.Items = category.Items.Where(x => x.Name.ToLower().Contains(filterString.Trim().ToLower())).ToList();
                    TempData["filterString"] = filterString;
                }
                return View(category);
            }
            catch
            {
                return NotFound();
            }
        }
    }
}
