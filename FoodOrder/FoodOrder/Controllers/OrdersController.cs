﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FoodOrder.Models;
using FoodOrder.Shared.Services;
using FoodOrder.Shared.Models;
using Microsoft.AspNetCore.Server.IIS;

namespace FoodOrder.Controllers
{
    public class OrdersController : Controller
    {
        private readonly IFoodOrderService service;

        public OrdersController(IFoodOrderService service)
        {
            this.service = service;
        }
        public IActionResult Index()
        {
            List<Item> items = new List<Item>();
            if (HttpContext.Session.GetString("Basket") != null)
            {
                items = HttpContext.Session.GetString("Basket")!.Split(',', StringSplitOptions.RemoveEmptyEntries).Select(x => service.GetItemById(int.Parse(x))).ToList();
            }
            int sum = 0;
            items.ForEach(x => sum += x.Price);
           return View(new ItemViewModel()
           {
               Items = items,
               Sum = sum
           });
        }
        public IActionResult AddToCart(int id,string returnUrl="")
        {
            Console.WriteLine(returnUrl);
            if (HttpContext.Session.GetString("Basket") == null)
            {
                HttpContext.Session.SetString("Basket","");
            }
            try
            {
                int sum = 0;
                List<string> items = HttpContext.Session.GetString("Basket")!.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList();
                if (items.Contains(id.ToString())) throw new InvalidDataException("The item is already added, due to lack of stock, you can buy only one!");
                items.ForEach(x => sum += service.GetItemById(int.Parse(x)).Price);
                if (sum + service.GetItemById(id).Price > 20000) throw new InvalidDataException("The price is too high! Max is 20.000!");
                items.Add(id.ToString());
                HttpContext.Session.SetString("Basket", String.Join(',', items));
            }
            catch (InvalidDataException ex)
            {
                TempData["Success"] = false;
                TempData["Message"] = ex.Message;
                return RedirecToLocal(returnUrl);
            }
            catch
            {
                TempData["Success"] = false;
                TempData["Message"] = "Unable to retrieve Item info!";
                return RedirecToLocal(returnUrl);
            }
            TempData["Success"] = true;
            TempData["Message"] = "Item successfully added to Cart!";
            return RedirecToLocal(returnUrl);
        }
        public IActionResult DeleteFromCart(int id)
        {
            List<string> items = HttpContext.Session.GetString("Basket")!.Split(',', StringSplitOptions.RemoveEmptyEntries).ToList();
            items.Remove(id.ToString());
            HttpContext.Session.SetString("Basket", String.Join(',', items));
            return RedirectToAction(nameof(Index));
        }
        public IActionResult DeleteAll()
        {
            HttpContext.Session.SetString("Basket", "");
            return RedirectToAction(nameof(Index));
        }
        public IActionResult Create()
        {
            if(HttpContext.Session.GetString("Basket")==null || HttpContext.Session.GetString("Basket") == "")
            {
                TempData["OrderError"] = true;
                return RedirectToAction(nameof(Index));
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(OrderViewModel vm)
        {
            if (ModelState.IsValid)
            {
                Order order = (Order)vm;
                order.Items = HttpContext.Session.GetString("Basket")!.Split(',', StringSplitOptions.RemoveEmptyEntries).Select(x => service.GetItemById(int.Parse(x))).ToList();
                if (service.AddOrder(order))
                {
                    TempData["Success"] = true;
                    TempData["Message"] = "Order successfully placed!";
                    HttpContext.Session.SetString("Basket", "");
                    return RedirectToAction(nameof(Index), "Categories");
                }
                else
                {
                    TempData["Success"] = false;
                    TempData["Message"] = "Unable to save to database!";
                    return RedirectToAction(nameof(Index), "Categories");
                }
            }
            return View(vm);
        }
        private IActionResult RedirecToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Categories");
            }
        }
    }
}

