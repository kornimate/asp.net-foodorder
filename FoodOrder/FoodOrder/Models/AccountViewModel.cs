﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Configuration;

namespace FoodOrder.Web.Models
{
    public class LoginViewModel
    {
		[Required(ErrorMessage = "This field is compulsory")]
		[DisplayName("UserName")]
        public string UserName { get; set; } = null!;

		[Required(ErrorMessage = "This field is compulsory")]
		[DisplayName("Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; } = null!;
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessage ="This field is compulsory")]
        [DisplayName("FullName")]
        public string FullName { get; set; } = null!;

		[Required(ErrorMessage = "This field is compulsory")]
		[DisplayName("UserName")]
		public string UserName { get; set; } = null!;

		[Required(ErrorMessage = "This field is compulsory")]
		[DisplayName("Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; } = null!;

		[Required(ErrorMessage = "This field is compulsory")]
		[DisplayName("Password Confirm")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string PasswordRepeat { get; set; } = null!;
    }
}
