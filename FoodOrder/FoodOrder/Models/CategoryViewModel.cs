﻿using FoodOrder.Shared.Models;

namespace FoodOrder.Models
{
    public class CategoryViewModel
    {
        public List<Category> Categories { get; set; }
        public List<Tuple<string,int,int>> MostPopular { get; set; }

        public CategoryViewModel()
        {
            Categories = new List<Category>();
            MostPopular = new List<Tuple<string,int,int>>();
        }
    }
}
