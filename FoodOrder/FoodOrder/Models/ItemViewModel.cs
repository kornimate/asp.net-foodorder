﻿using FoodOrder.Shared.Models;

namespace FoodOrder.Models
{
    public class ItemViewModel
    {
        public List<Item> Items { get; set; }
        public int Sum { get; set; } = 0;

        public ItemViewModel()
        {
            Items = new List<Item>();
        }
    }
}
