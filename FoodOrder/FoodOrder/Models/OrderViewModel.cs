﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.Web.CodeGeneration.EntityFrameworkCore;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using FoodOrder.Shared.Models;

namespace FoodOrder.Models
{
    public class OrderViewModel
    {
        public OrderViewModel()
        {
            Items = new List<Item>();
        }
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is compulsory!")]
        [MaxLength(30, ErrorMessage = "Max 30 chars!")]
        [MinLength(5,ErrorMessage ="Name must be at least 5 chars!")]
        public string Name { get; set; } = null!;

        [Required(ErrorMessage = "Address is compulsory!")]
        [RegularExpression(@"^(\d{4})\s+([^\d]+),\s+([^\d]+)\s+(\d+(?:\/[^\d\s]*)?)\.$", ErrorMessage = "Address pattern is invalid!")]
        public string Address { get; set; } = null!;
        
        [Required(ErrorMessage = "Phone number is compulsory!")]
        [RegularExpression(@"^(\+?36)?[ -]?(\d{1,2}|(\(\d{1,2}\)))/?([ -]?\d){6,7}$", ErrorMessage ="Phone number pattern is invalid!")]
        public string PhoneNumber { get; set; } = null!;
        public bool Fulfilled { get; set; } = false;
        public virtual List<Item> Items { get; set; }

        public static explicit operator Order(OrderViewModel vmodel) => new Order()
        {
            Name = vmodel.Name,
            Address = vmodel.Address,
            PhoneNumber = vmodel.PhoneNumber,
            Fulfilled = vmodel.Fulfilled,
            Items = vmodel.Items,
            RecordedTime = DateTime.Now
        };
        public static explicit operator OrderViewModel(Order model) => new OrderViewModel()
        {
            Name = model.Name,
            Address = model.Address,
            PhoneNumber = model.PhoneNumber,
            Fulfilled = model.Fulfilled,
            Items = model.Items
        };
    }
}

