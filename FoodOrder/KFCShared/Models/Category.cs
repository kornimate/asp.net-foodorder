﻿using System.ComponentModel.DataAnnotations;

namespace FoodOrder.Shared.Models
{
    public class Category
    {
        public Category()
        {
            Items  =new HashSet<Item>();
        }

        [Key]
        public int Id { get; set; }

        [MaxLength(30)]
        public string Name { get; set; } = null!;

        [Required]
        public virtual ICollection<Item> Items { get; set; }
    }
}
