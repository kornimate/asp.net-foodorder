﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace FoodOrder.Shared.Models
{
	public class DbInitializer
	{
		private static FoodOrderDbContext context = null!;
		private static UserManager<ApplicationUser> userManager = null!;
		public static void Initialize(IServiceProvider serviceProvider)
		{
			context = serviceProvider.GetRequiredService<FoodOrderDbContext>();
			userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();

			context.Database.Migrate();

			if (!context.Users.Any())
			{
				try
				{
					var user = new ApplicationUser { FullName = "Admin", UserName = "Admin" };
					userManager.CreateAsync(user,"Admin").Wait();
				}
				catch { }
			}

			if(context.Categories.Any()) { return; }

			IList<Category> deflists = new List<Category>()
			{
				new Category()
				{
					Name="Soups",
					Items = new List<Item>()
					{
						new Item()
						{
							Name="Fruit Soup",
							Description="A regular fruit soup",
							Price=2100,
							Spicy=false,
							Vegetarian=true,
						},
						new Item()
						{
							Name="Vegetable Soup",
							Description="A regular vegetable soup",
							Price=1500,
							Spicy=false,
							Vegetarian=true,
						},
						new Item()
						{
							Name="Chicken Soup",
							Description="A regular chicken soup",
							Price=2000,
							Spicy=false,
							Vegetarian=false,
						}
					}
				},
				new Category()
				{
					Name="Still Drinks",
					Items = new List<Item>()
					{
						new Item()
						{
							Name="Water",
							Description="Simple still water",
							Price=300,
							Spicy=false,
							Vegetarian=true,
						},
						new Item()
						{
							Name="Orange Juice",
							Description="Made from fresh oranges",
							Price=900,
							Spicy=false,
							Vegetarian=true,
						},
						new Item()
						{
							Name="Apple juice",
							Description="Made from fresh apples",
							Price=900,
							Spicy=false,
							Vegetarian=true,
						}
					}
				},
				new Category()
				{
					Name="Main Dishes",
					Items = new List<Item>()
					{
						new Item()
						{
							Name="Beef with French Fries",
							Description="A huge amount of food for hardly any money",
							Price=9000,
							Spicy=false,
							Vegetarian=false,
						},
						new Item()
						{
							Name="Steak",
							Description="The best Steak in Town",
							Price=15000,
							Spicy=false,
							Vegetarian=false,
						},
						new Item()
						{
							Name="Tofu and rice",
							Description="A good choice for vegetarians",
							Price=8000,
							Spicy=false,
							Vegetarian=true,
						}
					}
				},
				new Category()
				{
					Name="Desserts",
					Items = new List<Item>()
					{
						new Item()
						{
							Name="Ice Cream",
							Description="Best in Summer",
							Price=100,
							Spicy=false,
							Vegetarian=true,
						},
						new Item()
						{
							Name="Pancake",
							Description="Good after every meal",
							Price=1500,
							Spicy=false,
							Vegetarian=true,
						},
						new Item()
						{
							Name="Hot Chili Lemon Cake",
							Description="Freshy drink from out kitchen",
							Price=2000,
							Spicy=true,
							Vegetarian=true,
						}
					}
				}
			};

			context.AddRange(deflists);
			context.SaveChanges();

			List<Order> orders = new List<Order>()
			{
			new Order
			{
				Name = "TestOrder",
				PhoneNumber = "+00000000000",
				Address = "now way, no street 1293",
				Fulfilled = false,
				RecordedTime = DateTime.Now,
				FulfilledTime = null,
				Items = (List<Item>)deflists[1].Items
			},
			new Order
			{
				Name = "TestOrder",
				PhoneNumber = "+00000000000",
				Address = "now way, no street 1293",
				Fulfilled = false,
				RecordedTime = DateTime.Now,
				FulfilledTime = null,
				Items = (List<Item>)deflists[1].Items
			},
		    new Order
			{
				Name = "TestOrder",
				PhoneNumber = "+00000000000",
				Address = "now way, no street 1293",
				Fulfilled = false,
				RecordedTime = DateTime.Now,
				FulfilledTime = null,
				Items = (List<Item>)deflists[1].Items
			},
		    new Order
			{
				Name = "TestOrder",
				PhoneNumber = "+00000000000",
				Address = "now way, no street 1293",
				Fulfilled = false,
				RecordedTime = DateTime.Now,
				FulfilledTime = null,
				Items = (List<Item>)deflists[1].Items
			},
		    new Order
			{
				Name = "TestOrder",
				PhoneNumber = "+00000000000",
				Address = "now way, no street 1293",
				Fulfilled = false,
				RecordedTime = DateTime.Now,
				FulfilledTime = null,
				Items = (List<Item>)deflists[1].Items
			},
			new Order
			{
				Name = "TestOrder",
				PhoneNumber = "+00000000000",
				Address = "now way, no street 1293",
				Fulfilled = false,
				RecordedTime = DateTime.Now,
				FulfilledTime = null,
				Items = (List<Item>)deflists[1].Items
			},
			new Order
			{
				Name = "TestOrder",
				PhoneNumber = "+00000000000",
				Address = "now way, no street 1293",
				Fulfilled = false,
				RecordedTime = DateTime.Now,
				FulfilledTime = null,
				Items = (List<Item>)deflists[1].Items
			},
			new Order
			{
				Name = "TestOrder",
				PhoneNumber = "+00000000000",
				Address = "now way, no street 1293",
				Fulfilled = false,
				RecordedTime = DateTime.Now,
				FulfilledTime = null,
				Items = (List<Item>)deflists[1].Items
			},
																											new Order
			{
				Name = "TestOrder",
				PhoneNumber = "+00000000000",
				Address = "now way, no street 1293",
				Fulfilled = false,
				RecordedTime = DateTime.Now,
				FulfilledTime = null,
				Items = (List<Item>)deflists[1].Items
			},
			};
			context.AddRange(orders);
			context.SaveChanges();
		}
	}
}
