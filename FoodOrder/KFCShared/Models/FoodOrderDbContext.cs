﻿using Microsoft.EntityFrameworkCore;
using FoodOrder.Shared.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace FoodOrder.Shared.Models
{
    public class FoodOrderDbContext : IdentityDbContext<ApplicationUser>
	{
        public DbSet<Item> Items { get; set; } = null!;
        public DbSet<Order> Orders { get; set; } = null!;
        public DbSet<Category> Categories { get; set; } = null!;

        public FoodOrderDbContext(DbContextOptions<FoodOrderDbContext> options):base(options) 
        {
            
        }
    }
}
