﻿using System.ComponentModel.DataAnnotations;

namespace FoodOrder.Shared.Models
{
    public class Item
    {
        public Item()
        {
            Orders = new List<Order>();
        }

        [Key]
        public int Id { get; set; }

        [MaxLength(30)]
        public string Name { get; set; } = null!;

        [DataType(DataType.MultilineText)]
        public string? Description { get; set; }
        public int Price { get; set; }
        public bool Spicy { get; set; }
        public bool Vegetarian { get; set; }
        public int CategoryId { get; set; }

        [Required]
        public virtual Category Category { get; set; } = null!;
        public virtual List<Order> Orders { get; set; } = null!;
    }
}
