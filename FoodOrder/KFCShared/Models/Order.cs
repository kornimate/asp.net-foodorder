﻿using System.ComponentModel.DataAnnotations;

namespace FoodOrder.Shared.Models
{
    public class Order
    {
        public Order()
        {
            Items = new List<Item>();
        }
        [Key]
        public int Id { get; set; }

        [MaxLength(30)]
        public string Name { get; set; } = null!;

        public string Address { get; set; } = null!;

        public string PhoneNumber { get; set; } = null!;

        public bool Fulfilled { get; set; }

        [Required]
        public DateTime RecordedTime { get; set; }

        public DateTime? FulfilledTime { get; set; }

        [Required]
        public virtual List<Item> Items { get; set; }

    }
}
