﻿using FoodOrder.Shared.Models;
using Microsoft.EntityFrameworkCore;

namespace FoodOrder.Shared.Services
{
    public class FoodOrderService : IFoodOrderService
    {
        private readonly FoodOrderDbContext context;

        public FoodOrderService(FoodOrderDbContext context)
        {
            this.context = context;
        }

        public bool AddOrder(Order order)
        {
            try
            {
                context.Orders.Add(order);
                context.SaveChanges();
            }
            catch
            {
                return false;
            }
            return true;
        }

		public Item? CreateItem(Item item,out bool existing)
		{
            existing = false;
            try
            {
                if(context.Items.SingleOrDefault(x => x.Name == item.Name)!=null)
                {
                    existing = true;
                    return null;
                }
                context.Items.Add(item);
                context.SaveChanges();
            }
            catch
            {
                return null;
            }
            return item;
		}

		public List<Category> GetCategories()
        {
            return context.Categories.Include(x =>x.Items).ToList();
        }
        public Category GetCategoryDetails(int id)
        {
            return context.Categories.Include(x => x.Items).Single(x => x.Id == id);
        }
        public Item GetItemById(int id)
        {
            try
            {
                return context.Items.Single(x => x.Id == id);
            }
            catch
            {
                return new Item() { Id = 0, Name = "Error", Description = "Error", Price = 0, Spicy = false, Vegetarian = false } ;
            }
        }
		public List<Order> GetOrders()
		{
            return context.Orders.ToList();
		}

		public Order GetOrderDetails(int id)
        {
            return context.Orders.Include(x => x.Items).SingleOrDefault(x => x.Id == id)!;
        }

		public bool SetOrderCompleted(int orderId)
		{
			var order = context.Orders.SingleOrDefault(x => x.Id == orderId);
            if (order is null) return false;
            order.Fulfilled = true;
            order.FulfilledTime = DateTime.Now;
            context.Orders.Update(order);
            context.SaveChanges();
            return true;
		}
	}
}
