﻿using FoodOrder.Shared.Models;

namespace FoodOrder.Shared.Services
{
    public interface IFoodOrderService
    {
        List<Category> GetCategories();
        Category GetCategoryDetails(int id);
        Item GetItemById(int id);

        bool AddOrder(Order order);
		Item? CreateItem(Item item,out bool existing);
		List<Order> GetOrders();
        Order GetOrderDetails(int id);
		bool SetOrderCompleted(int orderId);
	}
}
